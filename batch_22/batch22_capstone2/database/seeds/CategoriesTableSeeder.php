<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('categories')->insert(
        [
      
          ['name'=>'Science','description'=>'Lorem Ipsum dolor','created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],
          ['name'=>'Mathematics','description'=>'Lorem Ipsum dolor','created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],
          ['name'=>'Social Science','description'=>'Lorem Ipsum dolor','created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')]

        //
        /*
            - insert an entry in the 'categories' table with the ff. key-value pairs:
                - 'name' => 'Science',
                  'description' => 'Lorem ipsum dolor'

                    - DB::?('?')->?([
                        '?' => '?',
                        '?' => '?'
                    ]);

            
            - insert an entry in the 'categories' table with the ff. key-value pairs:
                - 'name' => 'Mathematics',
                  'description' => 'Lorem ipsum dolor'

                    - DB::?('?')->?([
                        '?' => '?',
                        '?' => '?'
                    ]);

            
            - insert an entry in the 'categories' table with the ff. key-value pairs:
                - 'name' => 'Social Science',
                  'description' => 'Lorem ipsum dolor'

                    - DB::?('?')->?([
                        '?' => '?',
                        '?' => '?'
                    ]);


        */
      ]
    );              
  }
}