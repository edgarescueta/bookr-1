<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        DB::table('users')->insert(
              [
                ['name' => 'Administrator',
                 'email' => 'admin@bookr.com',
                 'password' => bcrypt('admin'),
                 'type' => 'admin',
                 'status' => 1
                ],

                ['name' => 'Homer',
                 'email' => 'user@bookr.com',
                 'password' => bcrypt('user'),
                 'type' => 'user',
                 'status' => 0
                ]               
              ]
            );

        /*
            - insert an entry in the 'users' table with the ff. key-value pairs:
                - 'name' => 'Administrator',
                - 'email' => 'admin@bookr.com',
                - 'password' => bcrypt('admin'),
                - 'type' => 'admin',
                - 'status' => '1'

                    - DB::?('?')->?([
                        '?' => '?',
                        '?' => '?',
                        '?' => '?',
                        '?' => '?',
                        '?' => '?'
                    ]);
        */


    }
}