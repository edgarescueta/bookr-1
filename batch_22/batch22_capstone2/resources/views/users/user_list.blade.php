@extends('layouts.app')

@section('title')
    User List
@endsection

@section('content')
<div class="container">
  <h3 class="text-center">Users</h3>

  <table>
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Type</th>
        <th>Status</th>
        <th>Actions</th>
        <tbody>
            @foreach($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->type }}</td>
            <td><button type="submit">Approve</button></td>
            <td><button type="submit">Edit User</button><button type="submit">Delete User</button>
          </td>
          </tr>
            @endforeach
        </tbody>
      </tr>
    </thead>
  </table>
  </div>

{{-- 
  1. Add a button for adding New User
  2. Create a table with the following table headers:
    - #
    - Name
    - Email
    - Type
    - Status
    - Actions
  3. For each row:
    @if the user's status == 0
      - Show the button for Approval
    @else
      - Show the buttons for Editing and Deleting a User
--}}
@endsection