@extends('layouts.app')

@section('title')
    Add Category Details
@endsection

@section('content')
  {{-- 
    1. Add a Form to Add Categories. Include Form Validations
      - Input for Category Name (required)
      - Input for Description
      - Button to Add a Category
  --}}
  <form action="/addcategories" method="POST" enctype="multipart/form-data">
    @csrf
        <h3 class="text-center">Add Category</h3>

        <div class="form-group">
          <label>Category Name:</label>
          <input type="text" name="bookname" class="form-control">
        </div>
        <div class="form-group">
          <label>Description/label>
          <textarea name="isbn" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>
     <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Add a Category</button>
@endsection