@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content') 

<div class="container">

<form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Find Book</button>
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href="/showbook">Browse</a></button>
    </form>

 </div>

 @endsection