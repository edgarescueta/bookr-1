@extends('layouts.app')

@section('title')
    Book Return Form
@endsection

@section('content')
  {{-- 
    1. Create a Form for returning a book
      - Input for Book Name
      - Input for Borrower Name
      - Input for Borrowed Date
      - Input for Return Date
      - Confirm Button
  --}}
  
  <div class="container">

  <form action="/returnbook" method="POST">
        @csrf
        <h3 class="text-center">Book Return Form</h3>

        <div class="form-group">
          <label>Book Name</label>
          <input type="string" name="bookname" class="form-control">
        </div>

        <div class="form-group">
          <label>Borrower Name</label>
          <input type="name" name="borrowername" class="form-control">
        </div>

        <div class="form-group">
          <label>Borrowed Date</label>
          <input type="date" name="borroweddate" class="form-control">
        </div>

        <div class="form-group">
          <label>Return Date</label>
          <input type="date" name="borroweddate" class="form-control">
        </div>

        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Confirm</button>
       
      </form>

    </div>

@endsection
