@extends('layouts.app')

@section('title')
    Library
@endsection

@section('content')
  {{--
    1. Add a Form with an input field called Search
    2. Show all the Books available in a form of a Card(Bootstrap Component)
      @for each books
        - Show the image
        - Show the Book Name
        - Display Book Availability
        - Show Action Buttons 
          - Details
          - Borrow
          - Return
    Notes:
      - Show a message that No Books were found if there are no books available or The Search query does not match any of the books available
  --}}
  
    <div class="container">
      <div class="row justify-content-center">
  <form action="/searchbook" class="form-inline">
    @csrf
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
    </div>


<div class="card" style="width: 18rem;">

@foreach($books as $book)

  <img src="{{ $book->image_path }}" class="card-img-top">
  <div class="card-body">
    <h5 class="card-title">Book Name:{{ $book->name }}</h5>
    <h3 class="card-title">Book Availability: {{ $book->available }}</h3>
    
    {{-- Button to trigger Modal --}}
    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#detailsModal">Details</button>

    {{-- Details Modal --}}
    {{-- Update modal form --}}
          <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Book Details</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <table>
                    <thead>
                      <td>Book Name</td>
                      <td>Category</td>
                      <td>ISBN</td>
                      <td>Quantity</td>
                      <td>Description</td>
                      
                      <tbody>
                        @foreach($books as $book)
                        <tr>
                          <td>{{ $book->name }}</td>
                          <td>{{ $book->category_id }}</td>
                          <td>{{ $book->isbn }}</td>
                          <td>{{ $book->quantity }}</td>
                          <td>{{ $book->descrition }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </thead>
                  </table>
                  
                <div class="modal-footer justify-content-center">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          </div>

          {{-- End of Details Modal --}}

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href="/borrowbook">Borrow</a></button>
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href="/returnbook">Return</a></button>
  </div>
</div>
@endforeach
</div>


@endsection