@extends('layouts.app')

@section('title')
    Book Borrow Form
@endsection

@section('content')
  {{-- 
    1. Create Form for borrowing books
      - Input for Book Name
      - Select for Borrower Name
      - Input for Borrowed Date
      - Confirm Button
  --}}
  <form action="\borrowform" method="GET" enctype="multipart/form-data">
    @csrf
        <h3 class="text-center">Borrow Books</h3>
        <div class="container">
        	<div class="form-group">
        		<label>Book Name:</label>
        		<input type="text" name="bookname" class="form-control">
     
        	</div>

        	<div class="form-group">
        		<label>Borrower Name:</label>
        		<input type="number" name="borrowername" class="form-control">
        	</div>

        	<div class="form-group">
        		<label>Borrowed Date:</label>
        		<input type="number" name="borroweddate" class="form-control">
        	</div>
		 <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Confirm</button>
        </div>
@endsection