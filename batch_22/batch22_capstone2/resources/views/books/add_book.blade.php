@extends('layouts.app')

@section('title')
    Add Book Details
@endsection

@section('content')
  {{-- 
    1. Create a Form for adding Books
      - Input for uploading book image
      - Input for ISBN
      - Input for Book Name
      - Select for Category
      - Input for the Number of Available Stocks
      - Textarea for Description
      - Submit Button
  --}}


  <form action="/addbooks" method="POST" enctype="multipart/form-data">
    @csrf
        <h3 class="text-center">Add Books</h3>
  <div class="container">
        <div class="form-group">
          <label>Book image:</label>
          <input type="file" name="bookimage" class="form-control">
        
        </div>

        <div class="form-group">
          <label>ISBN:</label>
          <input type="number" name="bookisbn" class="form-control">
        </div>

        <div class="form-group">
          <label>Book Name:</label>
          <input type="text" name="bookname" class="form-control">
        </div>

        <div class="form-group">
          <label>Category:</label>
          <select class="form-control" name="bookcategory" id="">
          @foreach($categories as $category)
          <option value="{{ $category->id }}"> {{ $category->name }}</option>
          @endforeach
         </select>
        </div>

         <div class="form-group">
          <label>Available stocks:</label>
          <input type="number" name="availablestock" class="form-control">
        </div>

          <div class="form-group">
          <label>Description:</label>
          <input type="text" name="bookdescription" class="form-control">
        </div>

        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Submit</button>

    
  </form>
</div>


@endsection