<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Book extends Model
{
    //
    use SoftDeletes;

   
    /*
        add protected properties:
            - $guarded (Array)
            - $dates (Array)
                - add one element called 
                'deleted_at'
    */
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    

    /*
        Create a public method called users
            -return $this and use its ff. methods:
                ->belongsToMany
                ->withPivot
                ->withTimestamps
    */
    public function users(){
        return $this->belongsToMany("\App\User", "users_book")->withPivot('user_id','book_id')->withTimestamps();
    }
    

    /*
        Create a public method called requests
            -return $this and use its ff. methods: 
                ->belongsToMany
                ->withPivot
                ->withTimestamps
    */
    public function bookrequests(){
        return $this->belongsToMany("\App\BookRequest", "book_requests")->withPivot('user_id', 'book_id')->withTimestamps();
    }

    /*
        Create a public method called user_borrower
            - return $this and use the ff. methods:
                ->users
                ->wherePivot
    */
    public function userborrower(){
        return $this->belongsToMany("\App\Users", "users_book")->withPivot('user_id','book_id')->withTimestamps();
    }
    

    /*
        Create a public method called category
            - return $this and use the ff. method:
                ->belongsTo
    */
    public function categories(){
        return $this->belongsToMany("\App\Category", "categories")->withPivot('name','description')->withTimestamps();
    }
}