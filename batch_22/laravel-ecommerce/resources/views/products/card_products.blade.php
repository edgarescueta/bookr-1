<div class="shop-products">
	<div class="row">
		@foreach($showproducts as $product)
		<div class="col-lg-4 col-md-4 mb-3">
			<div class="card">
				<img src="" class="card-img-top" alt="">
				<div class="card-body">
					<p class="card-category-title mb-0 text-uppercase">{{ $product->name }}</p>
					<h5 class="card-title mb-1 text-secondary">{{ $product->description }}</h5>
					<p class="card-text text-danger mb-1">&#8369; {{ number_format($product->price, 2) }} </p>
					<!-- Add to cart form -->
					<form method="POST" action="/catalog/addtocart/{{ $product->id }}">
						@csrf
						<div class="form-group row">
							<label class="col-sm-5 col-form-label col-form-label-sm">Quantity:</label>
							<input type="number" name="quantity" class="form-control col-sm-7">
						</div>
						<div class="form-group row justify-content-center">
							<button type="submit" class="form-control btn btn-outline-primary btn-sm">Add to Cart</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>