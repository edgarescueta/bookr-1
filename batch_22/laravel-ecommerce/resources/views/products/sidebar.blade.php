<div class="sticky-top">
  <div class="accordion mb-3" id="categories">
    <div class="card">
      <div class="card-header py-0 bg-secondary" id="categories-heading">
        <h2 class="mb-0">
          <button class="btn btn-link d-block text-left w-100 text-white" type="button" data-toggle="collapse" data-target="#categories-heading-collapse" aria-expanded="true" aria-controls="categories-heading-collapse">
            Categories
          </button>
        </h2>
      </div>

      <div id="categories-heading-collapse" class="collapse show" aria-labelledby="categories-heading" data-parent="#categories">
        <div class="card-body py-0">
          <div class="card border-0">
            <ul class="list-group list-group-flush py-2">
              <li class="list-group-item border-0 py-1"><a href="/catalog">All</a></li>
              @foreach($showcategories as $category)
                <li class="list-group-item border-0 py-1">
                  <a href="/catalog/filter/{{ $category->id }}">{{ $category->name }}</a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>