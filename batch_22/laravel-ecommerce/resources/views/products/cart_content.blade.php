@extends('template')
@section('title', 'Catalog')
@section('content')
<div class="container-fluid mt-4">
	<div class="row justify-content-center">
		<div class="col-md-10">
			@if($products_in_cart != null)
			<table class="table">
				<thead>
					<tr>
						<th>Product</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>

					</tr>
				</thead>
				<tbody>

					@foreach($products_in_cart as $product)
					<tr>
						<td>{{ $product->name }}</td>
						<td>&#8369; {{ number_format($product->price, 2) }} </td>
						<td>
							<form class="form-inline" method="POST" action="/catalog/updatequantity/{{ $product->id }}">
								@csrf
								@method('PUT')
								<input type="text" name="newquantity" class="form-control mb-2 mr-sm-2" id="inlineFormquantity" placeholder="Quantity" value="{{ $product->quantity }}">
								<button type="submit" class="btn btn-primary mb-2">Update quantity</button>
							</form>
						</td>
						<td>
							<form class="form-inline" action="/catalog/removecart/{{ $product->id }}" method="POST">
								@csrf
								@method('DELETE')
								&#8369; {{ number_format($product->subtotal, 2) }}
								<br>
								<button class="btn btn-danger">Remove</button>
							</form>
						</td>
					</tr>
					@endforeach

					<tr>
						<td>
							<strong>TOTAL</strong>: Php {{ $total }}.00
						</td>
						<td></td><td></td><td></td>
					</tr>
					<tr>
						<td>
							<a href="/catalog/emptycart" class="btn btn-danger">Empty Cart</a>

							<br>
							<br>
							
						</td>
						<td></td>
						<td class="text-right">
								<small>Payment options:</small>
							
						</td>
						<td>
							<form action="/checkout" method="POST" class="form-inline">
								@csrf
								<a href="/checkoutCOD" class="btn btn-warning mx-2">Cash on delivery</a>
								<small> or </small>
								<input type="number" value={{ $total }} name="amount" hidden>
								<button class="btn btn-primary mx-2"><i class="fab fa-paypal"></i>aypal</button>
								
							</form>
							{{-- <section class="pay-area">
								<div>
									<img height="60" src="{{ asset('paypal-logo.png') }}">
									@if (session('error') || session('success'))
									<p class="{{ session('error') ? 'error':'success' }}">
										{{ session('error') ?? session('success') }}
									</p>
									@endif
									<form method="POST" action="{{ route('create-payment') }}">
										@csrf
										<div class="m-2">
											<input type="text" name="amount" placeholder="Amount">
											@if ($errors->has('amount'))
											<span class="error"> {{ $errors->first('amount') }} </span>
											@endif
										</div>
										<button>Pay Now</button>
									</form>
								</div>
							</section> --}}
						</td>
					</tr>
				</tbody>
			</table>

			@else
			<div class="jumbotron">
				<h1 class="display-4">Cart is empty!</h1>
				
				<a href="/catalog" class="btn btn-primary btn-lg">Go back to shopping</a>	
			</div>
			
			@endif

		</div>
	</div>
</div>

@endsection