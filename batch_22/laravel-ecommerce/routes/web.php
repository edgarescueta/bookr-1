<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//admin routes

Route::get('/admin', 'AdminController@showProducts');
Route::post('/addproduct', 'AdminController@addProducts');
Route::put('/updateproduct/{id}', 'AdminController@updateProduct');
Route::delete('/deleteproduct/{id}', 'AdminController@deleteProduct');


//client routes
Route::get('/catalog', 'ProductController@index')->name('catalog');
Route::get('/catalog/filter/{catId}', 'ProductController@filter');
Route::post('/catalog/addtocart/{prodId}', 'ProductController@store');
Route::get('/catalog/showcart', 'ProductController@show')->name('showcart');
Route::put('/catalog/updatequantity/{prodId}', 'ProductController@update');
Route::delete('/catalog/removecart/{prodId}', 'ProductController@destroy');
Route::get('/catalog/emptycart', 'ProductController@emptycart');
Route::get('/checkoutCOD', 'ProductController@cashOnDelivery');
Route::get('/transactions', 'ProductController@showHistory');
// Route::post('/checkoutPaypal', 'ProductController@checkoutpaypal');

// Route::view('/checkout', 'products.check_outmessage');
Route::post('/checkout', 'PaypalController@createPayment')->name('create-payment');
Route::get('/confirm', 'PaypalController@confirmPayment')->name('confirm-payment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
