<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Session;
class AdminController extends Controller
{
	public function showProducts(){
		$categories = Category::all();
		$products = Product::all();
		return view('admin.products', compact('products', 'categories'));
	}

	public function addProducts(Request $request){
		// simple back end validation
		$rules = [
			"productname" => "required",
			"productdescription" => "required",
			"productprice" => "required",
			"productcategory" => "required"
		];

		$this->validate($request, $rules);

		$newproduct = new Product;
		$newproduct->name = $request->productname;
		$newproduct->description = $request->productdescription;
		$newproduct->price = $request->productprice;
		$newproduct->category_id = $request->productcategory;
		// dd($newproduct);

			//file handling
		$image = $request->file('image');
		$image_name = time(). "." . $image->getClientOriginalExtension();
		$destination = "images/";
		$image->move($destination, $image_name);

		$newproduct->img_path = $destination.$image_name;

		$newproduct->save();
		Session::flash("success_msg", "Product added successfully!");
		return redirect('/admin');
	}

	public function updateProduct(Request $request, $id){
		$updateproduct = Product::find($id);
		$updateproduct->name = $request->updateproductname;
		$updateproduct->description = $request->updateproductdescription;
		$updateproduct->price = $request->updateproductprice;
		$updateproduct->category_id = $request->updateproductcategory;
		Session::flash("success_msg", "Product updated successfully!");
		$updateproduct->save();
		return redirect('/admin');
	}

	public function deleteProduct($id){
		$deleteproduct = Product::find($id);
		$deleteproduct->delete();
		Session::flash("delete_msg", "Product deleted!");
		return redirect('/admin');
	}
}
