<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	//a product belongs to one category
    public function category(){
    	return $this->belongsTo("\App\Category");
    }

    // public function orders(){
    // 	return $this->belongsToMany("\App\Order", "product_order")->withPivot("price", "quantity")->withTimeStamps();
    // }
}
