<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	//a category has many products
    public function products(){
    	return $this->hasMany("\App\Product");
    }
}
