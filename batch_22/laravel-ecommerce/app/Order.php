<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	//An order belongs to only one status
    public function status(){
    	return $this->belongsTo("\App\Status");
    }

    //many-to-many relationship
    public function products(){
    	/*many orders belongs to many products*/
    	// product_order -> is pivot table, meaning this is where the two main tables (products table & orders table) meet, it only contains the id of the tables to add column, use withPivot() and to add timestamps use withTimestamps()
    	return $this->belongsToMany("\App\Product", "product_order")->withPivot('price', 'quantity')->withTimestamps();
    }
}
