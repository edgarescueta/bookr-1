<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('products')->insert(
    		[
    			['name'=> 'Plain white shirt', 'price' => 120.00, 'category_id' => 1 ,'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Blue printed long sleeve', 'price' => 340.00, 'category_id' => 2, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Black w/ gray coat jacket', 'price' => 699.00, 'category_id' => 3, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Blue jeans jacket,', 'price' => 120.00, 'category_id' => 4, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Gray skinny pants', 'price' => 120.00, 'category_id' => 5, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Navy blue school skirt', 'price' => 120.00, 'category_id' => 6, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Long maong skirt', 'price' => 120.00, 'category_id' => 7, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Pink Floral printed blouse', 'price' => 120.00, 'category_id' => 8, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Black spaghetti strap sando', 'price' => 120.00, 'category_id' => 9, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Tomy maroon croptop', 'price' => 120.00, 'category_id' => 10, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Maong shorts', 'price' => 120.00, 'category_id' => 11, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Sweat pants/leggings', 'price' => 120.00, 'category_id' => 12, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Sweat pants/leggings', 'price' => 120.00, 'category_id' => 13, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Checkered polo', 'price' => 120.00, 'category_id' =>14 , 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],

    			['name'=> 'Tomy blue polo shirt', 'price' => 120.00, 'category_id' => 15, 'description' => 'lorem ipsum', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],


    		]
    	);
    }
}
